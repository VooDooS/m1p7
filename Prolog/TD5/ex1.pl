% EX 1 : On choisi une liste

% EX 2 :

move2([X|L], [Y|L]) :- X > 0, Y is X - 1.
move2([X|L], [Y|L]) :- P is X-1, P > 0, move2([P|L], [Y|L]).
move2([X|L], [X|G]) :- move2(L, G).



gagne([0,0,0,0]).
gagne(A) :- move2(A,B), \+ gagne(B).

gagne2([0,0,0,0]).
gagne2(A) :- move2(A,B), \+ gagne2(B), asserta( gagne2(P) ).


playermove([X,Y,Z,T],1,B,[U,Y,Z,T]) :- U is X - B.
playermove([X,Y,Z,T],2,B,[X,U,Z,T]) :- U is Y - B.
playermove([X,Y,Z,T],3,B,[X,Y,U,T]) :- U is Z - B.
playermove([X,Y,Z,T],4,B,[X,Y,Z,U]) :- U is T - B.



jeu([0,0,0,0]) :- write('Vous avez gagné !').
jeu(A) :- write('Numéro de rangée ? '), read(X),
	      write('Combien d''allumettes ?'), read(Y),
	      playermove(A,X,Y,B), jeu(B).