% EX 1.1

genereaux(0, []) :- !.
genereaux(N, [N|T]) :- M is N - 1, genereaux(M, T).

% Correction
aux(1,[1]).
aux(N,L) :- N>1, P is N-1, aux(P, R), append(R, [N], L).

genere(N, L) :- M is N*N, genereaux(M,L).



% EX 1.2

perm([],[]).
perm([H|L], Z) :- perm(L, W), insertion(H, W, Z).

insertion(X, L, [X|L]).
insertion(X, [Y|L], [Y|G]) :- insertion(X, L, G).


% EX 1.3

nombre_magique(N, M) :- M is N*(N*N + 1)//2.

somme_ligne(L, N, I, R) :-