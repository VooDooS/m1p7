/* EX 1.1 */

ue(ia, 6, 1).
ue(bd, 6, 1).
ue(ca, 6, 1).
ue(lo, 6, 1).
ue(sy, 6, 1).
ue(co, 6, 1).
ue(pc, 6, 1).
ue(al, 6, 1).
ue(cc, 6, 1).
ue(pr, 6, 1).
ue(gl, 6, 2).
ue(ig, 6, 2).
ue(aa, 6, 2).
ue(in, 6, 2).
ue(pf, 6, 2).
ue(te, 3, 2).
ue(pa, 6, 2).
ue(sm, 6, 2).
ue(av, 6, 2).
ue(di, 3, 2).
ue(ll, 3, 2).
ue(nt, 3, 2).
ue(th, 6, 2).
ue(se, 6, 2).
ue(pp, 6, 2).
ue(tr, 6, 2).
ue(an1, 3, 1).
ue(an2, 3, 2).

/* EX 1.2 */

parcours(lp, [lo,co,pc,gl,pf], [bd,ca,ps,al,cc,ig,tc,pa,sm,di,ll,nt,te]).
parcours(sri, [bd,sy,al], [ca,co,pr,cc,gl,te,di,ll,ig,in,tc,se,pp]).
parcours(mpri, [co,al,cc,tr], [pc,aa,tc,ti,in,pa,sm,av]).


/* EX 1.3 */

aux([]).
aux([X|L]) :- X \== an1, X \== an2, aux(L).

anglais([H|L]) :- (H == an1; H == an2), aux(L).
anglais([H|L]) :- H \== an1, H \== an2, anglais(L).

/* anglais2(L) :- occurrence(L, an1, 1), occurrence(L, an2, 0).
anglais2(L) :- occurrence(L, an2, 1), occurrence(L, an1, 0). */

/* anglais3([H|L]) :- (H == an1; H == an2), !, aux(L).
anglais3([H|L]) :- anglais3(L). */

/* EX 1.4 */
aux2([]).
aux2([H|L]) :- H \== se, H \== pp, H \== tr, aux2(L).

fin_etudes([H|L]) :- (H == se; H == pp; H == tr), aux2(L).
fin_etudes([H|L]) :- H \== se, H \== pp, H \== tr, fin_etudes(L).

/* EX 1.5 */
ects_sem(N, [], 0).
ects_sem(N, [H|P], E) :- ects_sem(N, P, M), ue(H,X,N), !, E is M + X.
ects_sem(N, [H|P], E) :- ects_sem(N, P, E).
