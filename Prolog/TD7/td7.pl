% EX 1%

variables([x(1):[1,2,3,4], x(2):[1,2,3,4], x(3):[1,2,3,4], x(4):[1,2,3,4]]).
consistants((x(I),VI),(x(J),VJ)) :- VI =\= VJ, I+VI =\= J+VJ, I-VI =\= J-VJ.

genere([],[]).
genere([X:L|S],[(X,V)|T]) :- member(V,L), genere(S,T).

teste([X1,X2]) :- consistants(X1,X2),!.
teste([X1,X2|L]) :- consistants(X1,X2), teste([X1|L]), teste([X2|L]).

%teste([(x(1),3),(x(2),1),(x(3),4),(x(4),2)]).%

genereEtTeste(Sol) :- variables(V), genere(V,Sol), teste(Sol).


% EX 2 %
teste2((_,_), []).
teste2((X, V), A) :- teste([(X,V)|A]).

%teste2((x(1),3),[(x(2),1),(x(3),4),(x(4),2)]).%

sra([], A, A) :- teste(A).
sra([X:D|L], SP, S) :- member(V,D),teste2((X,V),SP),sra(L,[(X,V)|SP],S).

sras(Sol) :- variables(V), sra(V, [], Sol).


% EX 3 %
construit(_,_,_:[],[]).
construit(X,V,X2:[V2|S],[V2|D2]) :- consistants((X,V),(X2,V2)), construit(X,V,X2:S,D2),!.
construit(X,V,X2:[_|S],D2) :- construit(X,V,X2:S,D2).

%construit(x(1),3,x(2):[1,2,3,4],D2).%

filtre(_,_,[],[]).
filtre(X,V,[A:D|S],[A:D2|VF]) :- construit(X,V,A:D,D2),filtre(X,V,S,VF).
% filtre(x(1), 1, [x(2):[1,2,3,4], x(3):[1,2,3,4], x(4):[1,2,3,4]], VarFiltrees). %

rcn([], S, S).
rcn([X:D|L], SP, S) :- member(V,D), filtre(X,V,L,LF), rcn(LF, [(X,V)|SP],S).

rcns(S) :- variables(V), rcn(V, [], S).

