pb_money([S,E,N,D,M,O,R,Y]) :-  fd_domain([E,N,D,O,R,Y], 0, 9),
				fd_domain([S,M], 1, 9),
				fd_all_different([S,E,N,D,M,O,R,Y]),
				(1000*(S+M) + 100*(E + O) + 10*(N + R) + D + E)
					#= (10000*M+1000*O+100*N+10*E+Y),
				fd_labeling([S,E,N,D,M,O,R,Y], []).



pb_reines([X1,X2,X3,X4]) :- fd_domain([X1,X2,X3,X4], 1, 4),
		 		fd_all_different([X1,X2,X3,X4]),
	X1 + 1 #\= X2 + 2, X1 + 1 #\= X3 + 3, X1 + 1 #\= X4 + 4,
	X2 + 2 #\= X3 + 3, X2 + 2 #\= X4 + 4,
	X3 + 3 #\= X4 + 4,
	X1 - 1 #\= X2 - 2, X1 - 1 #\= X3 - 3, X1 - 1 #\= X4 - 4,  
        X2 - 2 #\= X3 - 3, X2 - 2 #\= X4 - 4,
        X3 - 3 #\= X4 - 4,
				fd_labeling([X1,X2,X3,X4], []).


pb_suedoku([X11,X12,X13,X14,
	    X21,X22,X23,X24,
	    X31,X32,X33,X34,
            X41,X42,X43,X44])
	:- fd_domain([X11,X12,X13,X14,
            	      X21,X22,X23,X24,
            	      X31,X32,X33,X34,
            	      X41,X42,X43,X44], 1, 4),
	   fd_all_different([X11,X12,X13,X14]),
           fd_all_different([X21,X22,X23,X24]),
           fd_all_different([X31,X32,X33,X34]),
           fd_all_different([X41,X42,X43,X44]),
	   
           fd_all_different([X11,X21,X31,X41]),           
	   fd_all_different([X12,X22,X32,X42]),                      
	   fd_all_different([X13,X23,X33,X43]),                                
	   fd_all_different([X14,X24,X34,X44]),

	   fd_all_different([X11,X22,X33,X44]),
	   fd_all_different([X41,X32,X23,X14]),

	   fd_all_different([X11,X12,X21,X22]),
	   fd_all_different([X31,X41,X32,X42]),
	   fd_all_different([X13,X14,X23,X24]),
	   fd_all_different([X33,X43,X34,X44]),

	   fd_labeling([X11,X12,X13,X14,
                      X21,X22,X23,X24,
                      X31,X32,X33,X34,
                      X41,X42,X43,X44], []).


diag(N, L) :-diag(N, L). 
reines(N, L) :- fd_all_different(L).
