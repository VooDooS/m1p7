lepb([X,Y,Z]) :- fd_domain([X], 2, 7),
			fd_domain([Y], 1, 5),
			fd_domain([Z], 1, 2),
			X #= 2*Y + Z,
			X + Y #= 6.
contrebandier(W,P,C) :-
      fd_domain([W,P,C],0,9),
      4*W + 3*P + 2*C #=< 9,
      15*W + 10*P + 7*C #>= 30,
      fd_labeling([W,P,C]).

conmax(W,P,C) :- fd_maximize(contrebandier(W, P, C), P).
conmin(W,P,C) :- fd_minimize(contrebandier(W, P, C), P).


mqchine([X, Y, Z]) :- 
	fd_domain([X,Y], 100, 2000),
	12*X + 24*Y #=< 24000,
	30*X + 24*Y #=< 29400,
	12 * X +20 * Y #= Z,
	fd_labeling([X,Y,Z],[]).


mqxmqchine([X,Y,Z]) :- fd_maximize(mqchine([X,Y,Z]), Z).

futoshiki([X11, X12, X13, X14, X15,
	X21, X22, X23, X24, X25,
	X31, X32, X33, X34, X45,
	X41, X42, X43, X44, X45,
	X51, X52, X53, X54, X55]) :- 
		fd_domain([X11, X12, X13, X14, X15,
        		X21, X22, X23, X24, X25,
	        	X31, X32, X33, X34, X45,
        		X41, X42, X43, X44, X45,
	        	X51, X52, X53, X54, X55], 1 , 5),
		fd_all_different([X11,X12,X13,X14,X15]),
           	fd_all_different([X21,X22,X23,X24,X25]),
           	fd_all_different([X31,X32,X33,X34,X35]),
           	fd_all_different([X41,X42,X43,X44,X45]),
		fd_all_different([X51,X52,X53,X54,X55]),
           
           	fd_all_different([X11,X21,X31,X41,X51]),           
           	fd_all_different([X12,X22,X32,X42,X52]),                      
           	fd_all_different([X13,X23,X33,X43,X53]),
		fd_all_different([X14,X24,X34,X44,X54]),
		fd_all_different([X15,X25,X35,X45,X55]),

		X11 #>= X12, X13 #>= X14,
		X14 #>= X15, X44 #>= X45,
		X51 #>= X52, X52 #>= X53,
		
		X21 #= 4, X25 #= 2, X33 #= 4, X45 #= 4,
		fd_labeling([X11, X12, X13, X14, X15,
        X21, X22, X23, X24, X25,
        X31, X32, X33, X34, X45,
        X41, X42, X43, X44, X45,
        X51, X52, X53, X54, X55], []).

occurence([],_,0).
occurence([X|S],E,R) :- X=:=E, occurence(S,E,M),R is M + 1,!.
occurence([X|S],E,R) :- occurence (S,E,R).

