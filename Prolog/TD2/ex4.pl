concat([], Y, Y).
concat([H|T], Y, [H|T2]) :- concat(T, Y, T2).

dernier(X, [X]).
dernier(X, [_|T]) :- dernier(X, T).

longueurpaire([]).
longueurimpaire([_]).
longueurpaire([X|T]) :- longueurimpaire(T).
longueurimpaire([X|T]) :- longueurpaire(T).

reverse([], []).
reverse([H|T], L) :- dernieR(h,l
