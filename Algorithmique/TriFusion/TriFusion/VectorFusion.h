//
//  VectorFusion.h
//  TriFusion
//
//  Created by Ulysse Gérard on 30/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TriFusion__VectorFusion__
#define __TriFusion__VectorFusion__

#include <iostream>
#include <vector>

class VectorFusion : public std::vector<int> {
private:
    VectorFusion triFusionAux(VectorFusion &V);
public:
    VectorFusion(int t[], int);
    VectorFusion(std::vector<int> = std::vector<int>());
    
    
    static const VectorFusion fusion(VectorFusion &,VectorFusion &);
    
    void triFusion();
    void aff() const;
};

#endif /* defined(__TriFusion__VectorFusion__) */
