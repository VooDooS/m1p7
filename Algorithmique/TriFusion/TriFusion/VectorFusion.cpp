//
//  VectorFusion.cpp
//  TriFusion
//
//  Created by Ulysse Gérard on 30/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "VectorFusion.h"
using namespace std;

VectorFusion::VectorFusion(int t[], int n){
    //On le rempli:
    for(int i=0; i < n; i++){
        this->push_back(t[i]);
    }
}

VectorFusion::VectorFusion(vector<int> V) : vector::vector<int>(V) {
}

const VectorFusion VectorFusion::fusion(VectorFusion &V1, VectorFusion &V2){
    int i1 = 0, i2 = 0;
   // cout << V1.size();
    VectorFusion V3(0, 0);
    while(i1 < V1.size() || i2 < V2.size()){
        if(i1 >= V1.size()){
            V3.push_back(V2[i2]);
            i2++;
        }
        else if(i2 >= V2.size()){
            V3.push_back(V1[i1]);
            i1++;
        }
        else if(V1[i1] <= V2[i2]){
            V3.push_back(V1[i1]);
            i1++;
        }
        else {
            V3.push_back(V2[i2]);
            i2++;
        }
    }
    
    
    return V3;
}

VectorFusion VectorFusion::triFusionAux(VectorFusion &V){
    VectorFusion V1, V2,VA, VB;
    
    for(int i=0; i < V.size(); i++){
        if(i < V.size()/2){
            VA.push_back(V[i]);
        }
        else{
            VB.push_back(V[i]);
        }
    }
    if(VA.size() > 0){
        V1 = VectorFusion::triFusionAux(VA);
    }
    if(VB.size() > 0){
        V2 = VectorFusion::triFusionAux(VB);
    }
    
    return VectorFusion::fusion(V1, V2);
}

void VectorFusion::triFusion() {
    VectorFusion V(*(this));
    *this = triFusionAux(V);
}

void VectorFusion::aff() const{
    for(int i=0; i < this->size(); i++){
        cout << (*this)[i] << " ";
    }
    cout << endl;
}