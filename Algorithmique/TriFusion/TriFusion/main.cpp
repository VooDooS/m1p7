//
//  main.cpp
//  TriFusion
//
//  Created by Ulysse Gérard on 30/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include <iostream>
#include "VectorFusion.h"

using namespace std;

int main(int argc, const char * argv[])
{

    int t1[] = { 56,70,75,101 };
    int t2[] = { 10,90,58, 11,12 };
    int n1 = 4, n2 = 5;
    
    VectorFusion v1(t1, n1), v2(t2, n2);
    
    // insert code here...
    v2.aff();
    v2.triFusion();
    v2.aff();
    return 0;
}

