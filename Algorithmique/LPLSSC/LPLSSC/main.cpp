//
//  main.cpp
//  LPLSSC
//
//  Created by Ulysse Gérard on 29/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include <iostream>
#include <string>
#include <vector>

using namespace std;


int LPLSSC(const string S, const string T){
    int sl = (int)S.length();
    int tl = (int)T.length();
    
    vector<vector<int>> tab( sl , vector<int>( tl , 0 ) );
    
    //Boucle principale:
    for(int i = 1; i < sl; i++){
        for(int j = 1; j < tl; j++){
            if(S[i] == T[j]) {
                //On incrémente la longueur de la ssc:
                tab[i][j] = tab[i-1][j-1] + 1;
            }
            //Le max des ssc précedentes:
            else if(tab[i][j-1] > tab[i-1][j]) {
                tab[i][j] = tab[i][j-1];
            }
            else {
                tab[i][j] = tab[i-1][j];
            }
        }
    }
    
    return tab[sl-1][tl-1];    
}

int main(int argc, const char * argv[])
{

    string S("algorithmique");
    string T("programmation");
    cout << LPLSSC(S,T) << endl;
    return 0;
}

