//
//  main.cpp
//  Hashtables
//
//  Created by Ulysse Gérard on 23/09/13.
//

#include <iostream>
#include <cmath>

#include "DoubleHashtable.h"

int main(int argc, const char * argv[])
{
    int n = 11;
    
    //Une table de taille 11:
    DoubleHashtable dht = DoubleHashtable(n);
    
    dht.insert(10);
    dht.insert(22);
    dht.insert(31);
    dht.insert(4);
    dht.insert(15);
    dht.insert(28);
    dht.insert(83);
    dht.insert(88);
    dht.insert(59);
    dht.insert(37);
    
    dht.del(22);
    dht.del(4);
    
    dht.search(59);
    
    return 0;
}

