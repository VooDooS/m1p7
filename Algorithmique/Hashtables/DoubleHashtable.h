//
//  DoubleHashtable.h
//  Hashtables
//
//  Created by Ulysse Gérard on 23/09/13.
//

#ifndef __Hashtables__DoubleHashtable__
#define __Hashtables__DoubleHashtable__

#include <iostream>
#include <cmath>

#include "Hashtable.h"

using namespace std;

/* Comme suggéré dans le Cormen on choisit:
    - Une table de hachage de taille 2^n
    - h2 : n -> { nombres impairs}
 
    Ainsi h2 (n) est premier avec la taille de la table et celle-ci est donc parcourue entièrement.
 
    Conventions:    case vide : -1
                    case supprimée: -2 */

class DoubleHashtable : public Hashtable
{
private:
    int hash(int n,  int i) const;    //Redéfinition de hash
    int hash2(int n) const;      //Fonction de hachage supplémentaire (la première étant Hashtable::hash)
    
public:
    DoubleHashtable(int n); //Constructeur
    
    //Redéfinitions:
    void insert(int n); //Méthode d'insertion
    int  search(int n) const; //Méthode de recherche
    void del(int n);    //Méthode de suppression
};

#endif /* defined(__Hashtables__DoubleHashtable__) */
