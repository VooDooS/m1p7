//
//  AbstractHashtable.h
//  Hashtables
//
//  Created by Ulysse Gérard on 25/09/13.
//

#ifndef __Hashtables__AbstractHashtable__
#define __Hashtables__AbstractHashtable__

/* La forme la plus générique d'une table de hachage, non instanciable ! */

class AbstractHashtable
{
protected:
    int m_n;    // Taille de la table
    int *m_t;   // Table de hashage
    virtual int hash(int) const = 0;    //Fonction de hashage
    
public:
    AbstractHashtable(int n);   //Constructeur
    
    virtual void insert(int n) = 0; //Méthode d'insertion
    virtual int  search(int n) const = 0; //Méthode de recherche
    virtual void del(int n) = 0;    //Méthode de suppression
    virtual void show() const = 0;        //Affiche la table
};

#endif /* defined(__Hashtables__AbstractHashtable__) */
