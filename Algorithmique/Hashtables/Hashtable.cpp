//
//  Hashtable.cpp
//  Hashtables
//
//  Created by Ulysse Gérard on 23/09/13.
//

#include "Hashtable.h"
using namespace std;

Hashtable::Hashtable(int n) : AbstractHashtable(n){
    //On initialise le tableau:
    
    m_t = new int[n];
}

Hashtable::~Hashtable() {
    //On détruit le tableau:
    delete [] m_t;
}

int Hashtable::hash(int n) const{
    //Hash le plus simple possible:
    return n%m_n;
}

void Hashtable::insert(int n){
    //On hash et on place:
    m_t[hash(n)] = n;
    show();
}

int Hashtable::search(int n) const {
    //Opération inverse:
    return m_t[hash(n)];
}

void Hashtable::del(int n){
    //On supprime:
    m_t[hash(n)] = NULL;
    show();
}

void Hashtable::show() const{
    cout << endl;
    for(int i=0; i < m_n; i++){
        if(m_t[i] < 0){
            cout << i << "() | ";
        }
        else {
            cout << i << "(" << m_t[i] << ") | ";
        }
    }
    cout << endl << endl;
}