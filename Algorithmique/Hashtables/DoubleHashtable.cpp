//
//  DoubleHashtable.cpp
//  Hashtables
//
//  Created by Ulysse Gérard on 23/09/13.
//

#include "DoubleHashtable.h"

DoubleHashtable::DoubleHashtable(int n) : Hashtable(n){
    
    //Taille, initialisée avec des "cases vides":
    for(int i=0; i < m_n; i++){
        m_t[i] = -1;
    }
}

int DoubleHashtable::hash(int n, int i) const{
    //Double hashage:
    return (Hashtable::hash(n) + i*hash2(n))%m_n;
}

int DoubleHashtable::hash2(int n) const{
    //Toujours impair, cf condition expliqué dans définition de la classe
    return (1 + n%(m_n-1));
}

void DoubleHashtable::insert(int n){
    //On hashe tant que l'on ne tombe pas sur une case vide:
    int i = 0, h = 0, vt = 0;
    
    do{
        h = hash(n,i);
        vt = m_t[h];
        i++;
        
        cout << "Case " << h << " contient " << vt << endl;
    }
    while(vt != n && vt >= 0 && i < m_n);
    
    if(i == m_n){
        //Si complet:
        cout << "Table pleine !" << endl;
    }
    else {
        if(vt == n){
            cout << "Element deja enregistre !" << endl;
        }
        else {
            //Si une case vide on insert:
            m_t[h] = n;
        }
    }
    show();
}

int DoubleHashtable::search(int n) const{
    //On parcours tant que l'on ne trouve pas la bonne valeur:
    int i = 0, h = 0, vt = 0;
    
    do{
        h = hash(n,i);
        vt = m_t[h];
        i++;
        
        cout << "Case " << h << " contient " << vt << endl;
    }
    while(vt != n && vt != -1 && i < m_n);

    if(vt != n){
        //Si complet:
        cout << "Element introuvable." << endl << endl;
        
        return -1;
    }
    else {
        cout << "Element trouve dans la case d'indice " << h << "." << endl << endl;
        return h;
    }
}

void DoubleHashtable::del(int n){
    //On recherche et on supprime:
    int id = search(n);
    
    if(id >= 0){
        m_t[id] = -2;
    }
    
    show();
}