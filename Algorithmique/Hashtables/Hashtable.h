//
//  Hashtable.h
//  Hashtables
//
//  Created by Ulysse Gérard on 23/09/13.
//

#ifndef __Hashtables__Hashtable__
#define __Hashtables__Hashtable__

#include <iostream>

#include "AbstractHashtable.h"

class Hashtable : public AbstractHashtable
{
protected:
    int hash(int n) const;    //Fonction de hashage
    
public:
    Hashtable(int n);   //Constructeur
    ~Hashtable();       //Destructeur
    
    void insert(int n); //Méthode d'insertion
    int  search(int n) const; //Méthode de recherche
    void del(int n);    //Méthode de suppression
    void show() const;        //Affiche la table
};

#endif /* defined(__Hashtables__Hashtable__) */
