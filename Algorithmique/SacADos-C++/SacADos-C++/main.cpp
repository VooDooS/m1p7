//
//  main.cpp
//  SacADos-C++
//
//  Created by Ulysse Gérard on 29/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include <iostream>
using namespace std;

void aff(int t[], int n) {
    for(int i = 0; i < n; i++){
        cout << t[i] << " ";
    }
    cout << endl;
}

int valMaxAvecRepetition(int W, int poids[], int val[], int n) {
    int K[W];
    
    //Initialisation de la première case:
    K[0] = 0;
    
    for(int w = 1; w <= W; w++){
        K[w] = 0; //On travaille directement sur K[w] plutôt que sur une var intermédiaire "valmax"
        
        //On cherche le max:
        for(int i = 0; i < n; i++) {
            if(poids[i] <= w) {
                int v = K[w - poids[i]] + val[i];
            
                //Plus grand ?
                if(v > K[w]) K[w] = v;
            }
        }
    }
    
    return K[W];
}


int main(int argc, const char * argv[])
{
    int n = 5;
    int poids[5] = {1, 2, 5, 6, 7};
    int val[5] = {1, 6, 18, 22, 24};
    
    cout << "Valeurmax= " << valMaxAvecRepetition(12, poids, val, n) << endl;
    
    
    return 0;
}

