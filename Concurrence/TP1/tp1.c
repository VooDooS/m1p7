
#include <stdio.h>
#include <pthread.h>

int cpt = 0;

void* compte(void *ptr) {
  int i;
  while(cpt < 100) {
    cpt++;
    printf("%d ",cpt);
  }
}

main()
{
  pthread_t t1, t2;

  pthread_create(&t1, NULL, compte, NULL);
  pthread_create(&t2, NULL, compte, NULL);

  pthread_join(t1, NULL);
  pthread_join(t2, NULL);
}
