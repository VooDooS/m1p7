#include <stdio.h>
#include <pthread.h>

//Version 5 philosophes:
#define NPHI 5

//Un thread par philosophe:
pthread_t phi[NPHI];

//Une baguette à côté de chaque assiette:
pthread_mutex_t bag[NPHI];


void* philo(void *ptrToInt) {
  int *idphi = (int*)ptrToInt;

  printf("Philo %d\n", idphi);

  //On demande le verrou sur les deux baguettes:
  pthread_mutex_lock(&bag[*idphi]);
  pthread_mutex_unlock(&bag[*idphi]);

  printf("Sortie\n");

}

main()
{
  int i = 0;
  int ids[NPHI];
  int* idsp[NPHI];


  //Initialisations:
  for(i = 0; i < NPHI; i++) {
    ids[i] = i;
    idsp[i] = &ids[i];

    pthread_mutex_init(&bag[i], NULL);
    pthread_create(&phi[i], NULL, philo, (void *) *idsp[i]);
  }

  //Attente:
  for(i = 0; i < NPHI; i++) {
    pthread_join(phi[i], NULL);
  }
}
