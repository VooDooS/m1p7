


(* le module Stream et la signature STREAM *)
module type STREAM = sig
  type 'a streamhead = Nil | Cons of 'a * 'a stream
  and 'a stream = 'a streamhead Lazy.t

  (* concatenation de streams *)
  val (++) : 'a stream -> 'a stream -> 'a stream

  (* un stream contenant les premiers n elements *)
  val take : int -> 'a stream -> 'a stream

  (* le stream sans les premiers n elements *)
  val drop : int -> 'a stream -> 'a stream
  val reverse : 'a stream -> 'a stream
end;;


let (!$) = Lazy.force (* abbreviation *)



module Stream : STREAM = struct
  type 'a streamhead = Nil | Cons of 'a * 'a stream
  and 'a stream = 'a streamhead Lazy.t

  (* concatenation *)
  let (++) s1 s2 = 
    let rec aux s = match !$s with
    | Nil -> !$s2
    | Cons (hd, tl) -> Cons (hd, lazy(aux tl))
  in lazy(aux s1)

  (* copie paresseuse des premier n elements *)
  let take n s = 
    let rec take' n s = match n, !$s with
    | 0, _ -> Nil
    | _, Nil -> Nil
    | _, Cons (hd, tl) -> Cons (hd, lazy (take' (n - 1) tl))
  in lazy(take' n s)

  (* copie du stream apres avoir n elements *)
  let drop n s =
    let rec drop' n s = 
      match n with 0 -> !$s
      | n -> match !$s with
             | Nil -> Nil
             | Cons (_, tl) -> (drop' (n - 1) tl) in
    lazy (drop' n s)

  (* retourne un stream, monolythique...! *)
  let reverse s =
    let rec reverse' acc s = match !$s with
      | Nil -> acc
      | Cons (hd, tl) -> 
           reverse' (Cons (hd, lazy acc)) tl in
    lazy (reverse' Nil s)

  let add x s = lazy (Cons(x,s))  
end


module type STREAMEXT = sig
  include module type of Stream
  val of_list : 'a list -> 'a stream
  val to_list : 'a stream -> 'a list 
  val push : 'a -> 'a stream -> 'a stream
  val pop : 'a stream -> ('a * 'a stream) option
  val map : ('a -> 'b) -> 'a stream  -> 'b stream
  val filter : ('a -> bool) -> 'a stream -> 'a stream
  val split : 'a stream -> 'a stream * 'a stream
  val join : 'a stream * 'a stream -> 'a stream
end


module Streamext : STREAMEXT = struct
  include Stream

  let rec of_list = function
    | [] -> lazy Nil
    | hd::tl -> lazy (Cons(hd,of_list tl))

  let rec to_list s = match Lazy.force s with 
    | Nil -> []
    | Cons(hd, tl) -> hd::(to_list (tl))

  let push obj s = lazy (Cons(obj, s))

  let pop s = match !$s with
    | Nil -> None
    | Cons(hd, tl) -> Some (hd, tl)

  (* On met le lazy devant pour retarder au maximum le force *)
  let rec map f s = lazy( match Lazy.force s with
    | Nil -> Nil
    | Cons(x, xs) -> (Cons(f x, map f xs)) )

  let rec filter f s = lazy ( match Lazy.force s with
    | Nil -> Nil
    | Cons(x, xs) -> if f x then  (Cons(x, filter f  xs)) else !$(filter f xs ))

  let split s = 
    let splitaux s n = (take n s,  drop (n+1) s) in
    splitaux s ((List.length (to_list s))/2)

  let rec join s =  match s with
      | (lazy Nil, s2) -> s2
      | (lazy (Cons(s1, s1s)), s2) -> 
         (join ( s1s, ( push s1 s2)))

end
