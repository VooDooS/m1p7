let rec somme_liste = function
    | [] -> 0
    | h::tail -> h + somme_liste tail;;

let somme_liste_term l = 
    let rec aux acc = function
       | [] -> acc
       | h::tail -> aux (h+acc) tail
    in aux 0 l;;

somme_liste [1;0;-5;10];;
somme_liste_term [1;0;-5;10];;

let rec fold_left f x = function
    | [] -> x
    | h::tail -> fold_left f (f x h) tail;; 

let fold_left_tr f x lx =
    let rec aux acc f = function
        | [] -> acc
        | h::tail -> aux (f acc h) f  tail
    in aux x f lx;;

let somme_liste_2 l =
    fold_left (+) 0 l;;

let somme_liste_3 l =
    fold_left_tr (+) 0 l;;

somme_liste_2 [1;0;-5;10];;
somme_liste_3 [1;0;-5;10];;

let scal l l' = fold_left_tr (+) 0 (List.map2 ( * ) l l');;

scal [1;2;3] [4;2;-1];;

let scal_2 l l' = 
    List.fold_left2 (fun a x y -> a + x*y) 0 l l';;

scal_2 [1;2;3] [4;2;-1];;
