type 'a tree =
    | Node of 'a tree * 'a tree
    | Leaf of 'a

type inttree = int tree;;
let (t1:inttree) = Node(Leaf(4), Node(Leaf(7), Leaf(9)));;

(* Renvoie la liste des feuilles de l'arbre *)
let rec leaves = function
    | Leaf v -> [v]
    | Node (a, b) -> leaves a @ leaves b;;

leaves t1;;

(* Version récursive terminale *)
let leaves_tr t =
    let rec aux acc = function
        | Leaf v -> v::acc
        | Node (g, d) -> aux (aux acc d) g
    in aux [] t;;

leaves_tr t1;;
