let rec concat l ll = match l with
    | [] -> ll
    | h::tail -> h::(concat tail ll);;

concat [1;2;3;4] [10;11;15];;

let rec flatten = function
    | [] -> []
    | l::tail -> concat l (flatten tail);;

flatten [[2];[];[3;4;5]];;

let rec rev = function
    | [] -> []
    | h::tail -> (rev tail)@[h];;

let revterm l =
    let rec aux acc = function
        | [] -> acc
        | h::tail -> (aux (h::acc) tail) in
    aux [] l;; 

rev [1;2;3];;
revterm [1;2;3];;
