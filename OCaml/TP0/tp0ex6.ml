type contact =
    | Tel of int
    | Email of string;;

let mon_tel = 0123456789;;
let mon_email = "gabriel.scherer@gmail.com";;

let est_ce_moi = function
    | Tel mon_tel -> true
    | Email mon_email -> true
    | _ -> false;;

est_ce_moi (Tel 0909897656);;
