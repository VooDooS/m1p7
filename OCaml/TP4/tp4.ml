(* EX 1.1 *)
let a = `B "foo" and b = `B 4;;
(* let l = [`B "foo"; `B 4];; *)


(* EX 1.2 *)
let f (a:[< `A > `A]) = match a with `A -> 4;;
f (`A);;


(* EX 1.3 *)
let l:[ `A of int | `B of string] list = [`A 4; `B "foo"];;
(*let l' = `C::l;;*)


(*EX 1.4 *)
`A::[`B];;
(`A:UNTYPE)::[`B];;



(*EX 1.5*)
let f:([ `A | `B ] -> [> `A | `B ])= function 
   `A -> `B
   | _ -> `A;;
f `C;;


(*EX 1.6*)
let f:[`A] -> [< `A] = function _ -> `A;;
let f:[< `A] -> [`A] = function _ -> `A;;



(* EX 2.1 *)
let zero = (`Nil, 0.0);;
let positive flo = if flo > 0. then (`Pos, flo) else invalid_arg "positive";;
let negative flo = if flo < 0. then (`Neg, flo) else invalid_arg "negative";;

let to_float = function
    | (`Nil, _ ) -> 0.0
    | (_, x ) -> x;;
    
let sqrt:[`Nil | `Pos]*float -> [> `Nil | `Pos]*float = function
    | (`Nil, _) -> (`Nil, 0.0)
    | (_, x) -> (`Pos, Pervasives.sqrt(x));; 

to_float (sqrt (positive 3.));;

let exp = function  (_, x) -> (`Pos, Pervasives.exp(x));;

let log = function 
    (`Pos, x) when x < 1. -> (`Neg, Pervasives.log(x))
    |(`Pos, 1.) -> (`Nil, 0.0)
    |(`Pos, x) -> (`Pos, Pervasives.log(x));;
    
let sqrt_log x = sqrt (log x);;


(* EX 2.2 *)
(* O BAH ON PE PA *)


(* EX 2.3 *)
