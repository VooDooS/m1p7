module type MONNAIE =
    sig
        type t
        val un : t
        val plus : t -> t -> t
        val prod : float -> t -> t
    end
;;


(*module Client : functor (M : MONNAIE) ->
	sig
		type t
		type monnaie = M.t
		val depot : t -> monnaie -> monnaie
		val retrait : t -> monnaie -> monnaie
	end

*)

module Banque : functor (M : MONNAIE) -> 
	sig
		type t
		type monnaie = M.t
		val depot : t -> monnaie -> monnaie
		val retrait : t -> monnaie -> monnaie
		val creer : unit -> t
	end
	
	=
	
	struct
		type t
		type monnaie = M.t
		let depot t e = 
	end
;;
	
