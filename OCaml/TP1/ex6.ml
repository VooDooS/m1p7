module type OUTPUT =
	sig
		type channel
		val char: channel -> char -> unit
		val substring: channel -> string -> int -> int -> unit
end
;;

module OUTBUF : OUTPUT with type channel = Buffer.t = 
	struct
		type channel = Buffer.t
		let substring = Buffer.add_substring
		let char = Buffer.add_char
	end
;;

module OUTCHAN : OUTPUT = 
	struct
		type channel = Pervasives.out_channel
		let substring = Pervasives.output
		let char = Pervasives.output_char
	end
;;