module type ADMIN_ORDI =
	sig
		type t
		val create : unit -> t 
		val start : t -> unit
		val read_state : t -> int
	end
;;

module type USER_ORDI =
	sig
		type t
		val read_state : t -> int
	end
;;


module Ordi  =
struct
	type t = int ref
	let create () = ref 0
	let start s = s:=1
	let read_state s = !s
end
;;

module Ordmin = (Ordi : ADMIN_ORDI with type t = Ordi.t);; 
module Order = (Ordi : USER_ORDI with type t = Ordi.t);; 


let ook = Ordmin.create ();;
Ordmin.start ook;;
Order.read_state ook;;

(* let eek = Order.create ();; *)