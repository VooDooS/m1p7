(*module type MONNAIE =
    sig
        type t
        val un : t
        val plus : t -> t -> t
        val prod : float -> t -> t
    end
;;

module Mfloat =
    struct
        type t = float
        let un = 1.
        let plus t1 t2 = t1 +. t2
        let prod f t1 = f*.t1
    end
;;

module Euro = (Mfloat : MONNAIE);;
module Dollar = (Mfloat : MONNAIE);;

let euro f = Euro.prod f Euro.un;;*)


module type MONNAIE =
    sig
        type t
        val un : t
        val plus : t -> t -> t
        val prod : float -> t -> t
    end
;;


module Euro =
    struct
        type t = float
        let un = 1.
        let plus t1 t2 = t1 +. t2
        let prod f t1 = f*.t1
    end
;;
    
module Dollar =
    struct
        type t = float
        let un = 1.
        let plus t1 t2 = t1 +. t2
        let prod f t1 = f*.t1
    end
;;


module type BureauDeChange =
    sig
        module A:MONNAIE
        module B:MONNAIE
        val con12 : A.t -> B.t
        val con21 : B.t -> A.t
    end
;;

module BureauDeChange =
    struct
        module Euro =
            struct
                type t = float
                let un = 1.
                let plus t1 t2 = t1 +. t2
                let prod f t1 = f*.t1
            end
        module Dollar =
            struct
                type t = float
                let un = 1.
                let plus t1 t2 = t1 +. t2
                let prod f t1 = f*.t1
            end
        let conv12 e = 1.55 *. e
        let conv21 d = (1. /. 1.55) *. d
    end
;;


(* #use "ex2.ml";; *)

