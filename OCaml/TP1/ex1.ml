module type MONNAIE =
    sig
        type t
        val un : t
        val plus : t -> t -> t
        val prod : float -> t -> t
    end
;;

module Mfloat =
    struct
        type t = float
        let un = 1.
        let plus t1 t2 = t1 +. t2
        let prod f t1 = f*.t1
    end
;;

module Euro = (Mfloat : MONNAIE);;
module Dollar = (Mfloat : MONNAIE);;

let a = Mfloat.plus (Mfloat.un) (Mfloat.un);;

(* Euro.plus Euro.un Dollar.un;;
    Marche pô CAR abstraction de type *)

let euro f = Euro.prod f Euro.un;;

euro 78923400000.;;

(* EX 3 *)
module type MPLUS =
    sig
        type t
        val plus : t -> t -> t
    end
;;

module EuroPlus = (Euro : MPLUS with type t = Euro.t);;
EuroPlus.plus Euro.un Euro.un;;


(* #use "ex1.ml";; *)

