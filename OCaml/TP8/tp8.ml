(*

Preuve par récurrence de Ex1 :

Problème: si telle quelle on se retrouve avec une liste vide dans l'hypothèse de récurrence, c'es trop faible.

On va donc prouver une propriété plus forte !

Pt l Pt r, rev_append l r = (rev l) @ r

*)
let split list =
    let rec aux i acc = function
      | [] -> List.rev acc, []
      | h :: t as l -> if i = 0 then List.rev acc, l
                       else aux (i-1) (h :: acc) t  in
    aux ((List.length list)/2) [] list;;

module DEQUE : sig
    type 'a queue

    val empty : 'a queue
    val is_empty : 'a queue -> bool
  
    (* insert, inspect, and remove the front element *)
    val cons : 'a -> 'a queue -> 'a queue
    val removefirst : 'a queue -> 'a * 'a queue (* raise Empty if queue is empty *)
    
    (* insert, inspect, and remove the rear element *)
    val snoc : 'a queue -> 'a -> 'a queue
    val removelast : 'a queue -> 'a * 'a queue (* raises Empty if queue is empty *)
end = struct
  type 'a queue = 'a list * 'a list
  let empty = ([],[])

  let is_empty = function 
    | ([],[]) -> true
    | _ -> false

  let cons a = function (l, ll) -> (a::l, ll)

  let rec removefirst = function
    | ([],[]) -> failwith "empty"
    | ([], l) -> let (h,t) = split l in removefirst(List.rev t, h)
    | (a::l, ll) -> (a,  (l, ll))

  let snoc q a = match q with (l, ll) -> (l, a::ll)
  let rec removelast = function
    | ([],[]) -> failwith "empty"
    | (l, []) -> let (h,t) = split l in removelast(h, List.rev t)
    | (l, a::ll) -> (a,(l, ll)) 
end;;


let q = DEQUE.empty;;
let q = DEQUE.cons 1 q
let q = DEQUE.cons 2 q;;

