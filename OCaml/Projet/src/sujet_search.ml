(** {1 Basic definitions} *)

type ('a, 'b) sum = Left of 'a | Right of 'b
(** An element of [('a, 'b) sum] is either a ['a] or a ['b] *)

type 'a stream = Stream of (unit -> ('a * 'a stream) option)
(** (possibly infinite) streams of values *)

(** you have to implement the functions below; this may not be very
    useful for the rest of the project, but consider this a warmup
    exercize *)
let nil = Stream (fun() -> None)
let cons elt str = Stream (fun() -> Some (elt, str))

(** lazy cons *)
let lcons elt f = cons elt (f())

let get = function Stream f -> f()
let rec take s n = match n with
  | 0 -> []
  | n -> match get s with
           | None -> []
           | Some (elt, str) -> elt::(take str (n-1))

let rec  drop n s = match n with
  | 0 -> s
  | _ ->  match get s with
    | None -> nil
    | Some(_, s') -> drop (n-1) s'

let number_stream = 
  let rec number_stream_aux n = Stream (fun() -> Some (n, number_stream_aux (n+1)))
  in number_stream_aux 0
(** the infinite stream [0,1,2,3...] *)

let rec stream_of_list l = Stream ( fun() -> match l with
  | [] -> None
  | hd::tl ->  Some (hd, stream_of_list tl));;



module Logic1 :
  sig
    type 'a search
    (** the type of search descriptions *)

    val solve : int -> 'a search -> 'a list
    (** [solve n prob] returns [n] solutions to the search problem
        [prob], or less if there aren't so many solutions *)

    val stream : 'a stream -> 'a search
    (** a stream can be seen as a search problems whose solutions are
        all the elements present in the stream *)

    val return : 'a -> 'a search
    (** a single value also defines a search problem (which has
        exactly this value as solution *)

    val fail : 'a search
    (** the search problem with no solution *)

    val map : ('a -> 'b) -> 'a search -> 'b search
    (** the solutions of [map f prob] are all the [f x] such that [x]
        is a solution of [prob]. *)

    val sum : 'a search -> 'b search -> ('a, 'b) sum search
    (** the solutions of [sum pa pb] are all the solutions of problem
        [pa], and all the solutions of problem [pb] *)

    val prod : 'a search -> 'b search -> ('a * 'b) search

    val guard : ('a -> bool) -> 'a search -> 'a search
    (** the solutions of [guard condition prob] are the solutions of
        [prob] that also satisfy [condition]. *)
  end  =
  struct
    type 'a search = 'a stream
    (** the type of search descriptions *)

    let solve n s = take s n
    (** [solve n prob] returns [n] solutions to the search problem
        [prob], or less if there aren't so many solutions *)

    let stream s = s
    (** a stream can be seen as a search problems whose solutions are
        all the elements present in the stream *)

    let return elt = Stream (fun() -> Some(elt, nil))
    (** a single value also defines a search problem (which has
        exactly this value as solution *)

    let fail = nil
    (** the search problem with no solution *)

    let rec map f s = Stream ( fun() ->  match get s  with
      | None -> None
      | Some(elt, str) -> Some (f elt, map f str))

    let rec sum pa pb = Stream ( fun() -> match get pa, get pb with
      | None, None -> None
      | None, Some(b,pb') -> Some(Right b, sum pa pb')
      | Some (a,pa'), None -> Some (Left a, sum pa' pb)
      | Some (a,pa'), Some(b,pb') -> Some (Left a,Stream(fun() -> Some(Right b, sum pa' pb'))))

    let prod sa sb =

      let next xmax ymax nx ny = match xmax, ymax with (* Xmax et Ymax representent la "derniere coordonnée disponible", ils valent -1 si les ensembles sont supposes infinis*)
	| -1, -1 ->(match nx with                                                                 (* Cas des deux ensembles supposés infinis *)
	    | 0 -> (ny + 1, 0)
	    | _ -> (nx - 1, ny + 1))
	| _, -1 -> (match nx with                                                                 (* Cas premier ensemble fini uniquement *)
	    | 0 -> (xmax, ny - xmax + 1)
	    | _ -> (nx - 1, ny + 1))
	| -1, _ -> if ny = ymax then (nx + ymax + 1, 0) else (nx - 1, ny + 1)                     (* Cas second ensemble fini uniquement *)
	| _, _ -> if ny = ymax then (xmax, ymax - (xmax - nx) + 1) else (nx - 1, ny + 1) in       (* Cas des deux ensembles fini *)

      let rec prod_aux sa sb ra rb xmax ymax nx ny =  match xmax, ymax with
	| -1, -1 -> (match get ra, get rb with (* Cas des deux ensembles supposés infinis *)
	    | Some (a, ra'), Some (b, rb') -> let (nx', ny') = next xmax ymax nx ny in
					      (match nx with 
						| 0 -> Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) sb xmax ymax nx' ny'))
						| _ -> Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) rb' xmax ymax nx' ny')))
	    | None, Some (_, _) -> prod_aux sa sb (drop (nx - 1) sa) (drop 1 sb)  (nx - 1) ymax (nx - 1) 1
	    | Some (_, _), None -> prod_aux sa sb (drop (ny + 1) sa) sb xmax (ny - 1) (ny + 1) 0
	    | _ -> nil)
	| _, -1 -> (match get ra, get rb with (* Cas premier ensemble fini uniquement *)
	   | Some (a, ra'), Some (b, rb') -> let (nx', ny') = next xmax ymax nx ny in
					      (match nx with
						| 0 -> Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) (drop ny' sb) xmax ymax nx' ny'))
						| _ -> Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) rb' xmax ymax nx' ny')))
	    | Some (_,_), None -> prod_aux sa sb (drop xmax sa) (drop (ny - xmax + 1) sb) xmax (ny - 1) xmax (ny - xmax + 1)
	    | _ -> nil)
	| -1, _ -> (match get ra, get rb with (* Cas second ensemble fini uniquement *)
	    | Some (a, ra'), Some (b, rb') -> let (nx', ny') = next xmax ymax nx ny in
					      if ny = ymax then Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) sb xmax ymax nx' ny'))
					      else Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) rb' xmax ymax nx' ny'))
	    | None, Some(_,_) -> prod_aux sa sb (drop (nx - 1) sa) (drop 1 sb) (nx - 1) ymax (nx - 1) 1
	    | _ -> nil)
	| _, _ -> (match get ra, get rb with  (* Cas des deux ensembles fini *)
	    | Some (a, ra'), Some (b, rb') -> let (nx', ny') = next xmax ymax nx ny in
					      if ny = ymax then Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) (drop ny' sb) xmax ymax nx' ny'))
					      else Stream ( fun() -> Some ((a,b), prod_aux sa sb (drop nx' sa) rb' xmax ymax nx' ny'))
	    | _ -> nil)

      
      in
      prod_aux sa sb sa sb (-1) (-1) 0 0

    let rec guard f prob =  match get prob with
      | None -> nil
      | Some (elt, strm) -> if f elt then Stream ( fun() -> Some (elt,guard f strm)) else guard f strm

  end;;

open Logic1

let strm = number_stream;;

let prob = stream strm;;

let probpair = guard (fun x -> x mod 2 = 0) prob;;

let probimpair = guard (fun x -> x mod 2 = 1) prob;;

let strfini = stream (stream_of_list [0;1;2])

let probtout = sum probpair probimpair;;

let l' = solve 20 probtout;;

let l = solve 50 probpair;;

let l'' =  solve 5 (stream (drop 4 number_stream));;

let l_xmax = solve 16 (prod strfini prob);;

let l_ymax = solve 16 (prod prob strfini);; 

let l_fini = solve 18 (prod strfini (stream (stream_of_list [0;1;2;3;4;5])));;



(*
    val prod : 'a search -> 'b search -> ('a * 'b) search

    val guard : ('a -> bool) -> 'a search -> 'a search
    (** the solutions of [guard condition prob] are the solutions of
        [prob] that also satisfy [condition]. *)

  end *)


(* Tests *)

let () =
  let open Logic1 in
  let number = stream number_stream in
  let posnum = guard (fun n -> n > 0) number in

  let pytha =
    (* we search for triples pythagorean triples (a, b, c):
         aÂ² + bÂ² = cÂ²
       to avoid duplication of solutions, we also request (a < b) *)
    let ab =
      prod posnum posnum
      |> guard (fun (a,b) -> a < b) in
    prod ab posnum
    |> guard (fun ((a,b),c) -> a*a+b*b=c*c) in

  solve 10 pytha |> List.iter
      (fun ((a,b),c) -> Printf.printf "%d 2 + %d 2 = %d 2\n" a b c) ;;
