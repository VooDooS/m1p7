(** {2 Second part: random computations} *)


module type PROB = sig
  type 'a dist

  val run : ('a -> 'a -> int) -> 'a dist -> ('a * float) list

  val return : 'a -> 'a dist

  (** an element chosen at random, with equal probability, in the
      given array *)
  val uniform : 'a array -> 'a dist

  (** [rand n] is a random number between 0 and (n-1) *)
  val rand : int -> int dist

  (** be careful to compute the probabilities of the product
      correctly *)
  val prod : 'a dist -> 'b dist -> ('a * 'b) dist
  val map : ('a -> 'b) -> 'a dist -> 'b dist

  val bind : 'a dist -> ('a -> 'b dist) -> 'b dist
end

module Prob : PROB = struct
  type 'a dist = 'a list

  let run comp dist = 
    let sortedist = List.sort comp dist in
    let rec convert total acc = function 
      | [] -> acc
      | hd::tl -> let (elt, card) = hd in convert total ((elt, (card /. total))::acc) tl in
    let rec run_aux dist prev card total acc = 
      if total = 0. then (match dist with
	| [] -> []
	| hd::tl -> run_aux tl hd 1. 1. acc)
      else (match dist with 
	| [] -> convert total [] ((prev, card)::acc)
	| hd::tl -> if hd = prev then run_aux tl hd (card +. 1.) (total +. 1.) acc
	  else run_aux tl hd 1. (total +. 1.) ((prev, card)::acc)) in
    run_aux sortedist (List.hd dist) 0. 0. []
  
  let return x = [x]

  let uniform a =
    let rec uniform_aux a acc i = if i = 0 then ((Array.get a 0)::acc)
      else uniform_aux a ((Array.get a i)::acc) (i - 1) in
    let length = Array.length a in uniform_aux a [] (length - 1)

  let rand n =
    let rec rand_aux acc n = match n with
      | 0 -> (0::acc)
      | _ -> rand_aux (n::acc) (n-1)
    in rand_aux [] (n-1)

  let prod a b =
    let rec prod_aux a b lb acc = match a with
      | [] -> acc
      | hda::tla -> (match lb with
	| [] -> prod_aux tla b b acc
	| hdb::tlb -> prod_aux a b tlb ((hda,hdb)::acc)) in
    prod_aux a b b []

  let map f d = List.map f d

  let bind dist f = List.concat(List.map f dist)
end


(** {2 Helper functions for array-manipulating code} *)


module PArray = Map.Make(struct
  type t = int
  let compare = compare
end)

(** conversion functions *)
let parray_of_array arr =
  let parray = ref PArray.empty in
  for i = 0 to Array.length arr - 1 do
    parray := PArray.add i arr.(i) !parray
  done;
  !parray

let array_of_parray parray =
  Array.init (PArray.cardinal parray)
    (fun i -> PArray.find i parray)

let natural_parray n = 
  let init = Array.make n 0 in
  for i = 1 to Array.length init - 1 do
    Array.set init i i
  done;
  parray_of_array init



(** length of a persistent array *)
let length = PArray.cardinal

(** swapping two indices of a persistent array *)
let swap i j parray =
  parray
  |> PArray.add i (PArray.find j parray)
  |> PArray.add j (PArray.find i parray)

(** Remark: feel free, of course, to add additional convenience
    functions, according to the needs of the code you'll develop. *)

open Prob

(** looping functions; you may want to look at their type *)
let rec for_to a b v f =
  if a > b then return v
  else
    bind (f a v) @@ fun v' ->
    for_to (a + 1) b v' f

let rec for_downto a b v f =
  if a < b then return v
  else
    bind (f a v) @@ fun v' ->
    for_downto (a - 1) b v' f

let rand_parray n =
  let init = parray_of_array (Array.make n 0) in
  for_to 0 n init (fun i parr ->
    bind (rand (i+1)) @@ fun v ->
    return (PArray.add i v parr))

let true_rand_parray n = 
  let init = parray_of_array (Array.make (n) 0) in
  for_to 0 (n - 1) init (fun i parr ->
    bind (rand (n)) @@ fun v ->
      return (PArray.add i v parr))

(* true_rand_parray n renvoie une dist contenant un tableau de taille n rempli aleatoirement par des entiers entre 1 et n - 1,
chaque sortie etant equiprobable *)

let combine a b =
  let n = Array.length a in
  let ret = Array.make n ((Array.get a 0), (Array.get b 0)) in
  for i = 1 to (n - 1) do Array.set ret i ((Array.get a i), (Array.get b i)) done;
  ret 

(* combine a b renvoie un tableau de taille n (n etant la taille de a ET de B) contenant les coupes (a[i],b[i])  *)

let left_of ab =
  let n = Array.length ab in
  let ret = Array.make n (let (x,_) = Array.get ab 0 in x) in
  for i = 1 to (n - 1) do Array.set ret i (let (x,_) = Array.get ab i in x) done;
  ret 
  
(*left_of ab renvoie un tableau contenant les membres gauches du tableau de couples ab*)

(** {2 Where you should go from here} *)

let shuffleAlgo1 (n:int) =
  let init = natural_parray n in
  
   (*  let bind dist f = List.concat(List.map f dist) *)
   (*  val bind : 'a dist -> ('a -> 'b dist) -> 'b dist *)
  bind (rand n) (fun _ ->
    bind (rand n) (fun i -> 
      bind (rand n) (fun j -> 
	return (swap i j init))))
;;

run compare (shuffleAlgo1 4 |> map array_of_parray);;

let shuffleAlgo2 (n:int) =
  let init = natural_parray n in
  let poids = true_rand_parray n in
  let tri valeurs poids =
  let temp = combine valeurs poids in
  Array.stable_sort (fun (_,p1) (_,p2) -> compare p1 p2) temp;
  left_of temp in
  map (fun p -> tri (array_of_parray init) (array_of_parray p)) poids
  
;;

run compare (shuffleAlgo2 4) ;;

let shuffleAlgo3 (n:int) =
  let init = natural_parray n in
  
   (*  let bind dist f = List.concat(List.map f dist) *)
   (*  val bind : 'a dist -> ('a -> 'b dist) -> 'b dist *)
  for_downto (n-1) 1 init (fun i parr ->
    bind (rand (i+1)) (fun j -> 
	return (swap i j parr)))
;;

run compare (shuffleAlgo3 4 |> map array_of_parray);;

(* test des focntions du module Prob *)

let pair = prod (rand 3) (rand 3)
let sum = map (fun (a,b) -> a + b) pair
let sum4 = map (fun (a,b) -> 4 * a + b) pair
let res_sum = run compare sum
let res_sum4 = run compare sum4

let arr =  Array.make 3 0
let u = Array.set arr 1 1; Array.set arr 2 2; uniform arr
let res_u = run compare u
let arr_bis = Array.make 4 0
let ruuun = Array.set arr_bis 1 1; Array.set arr_bis 2 1; Array.set arr_bis 3 2; uniform arr_bis
let forrest = compare
let rezz = run forrest ruuun


