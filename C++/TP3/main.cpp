//
//  main.cpp
//  TP2-2013
//
//  Created by Ulysse Gérard on 28/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include <iostream>

#include "Date.h"


int main(int argc, const char * argv[])
{
    Date dat(30,05,1990);
    
    dat.afficher();
    Date dat2 = dat.lendemain();
    dat2.afficher();

    
    return 0;
}