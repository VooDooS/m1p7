//
//  Duree.cpp
//  TP2-2013
//
//  Created by Ulysse Gérard on 28/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Duree.h"
using namespace std;

Duree::Duree() {
    m_njours = 0;
}

Duree::Duree(int nj) : m_njours(nj) {};

void Duree::multiplierPar(int n){
    m_njours *= n;
}

int Duree::getj() const{
    return m_njours;
}
void Duree::afficher() const {
    cout << m_njours/365 << " annees et " << m_njours%365 << "jours" << endl;
}