//
//  Date.cpp
//  TP2-2013
//
//  Created by Ulysse Gérard on 28/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Date.h"

using namespace std;

Date::Date(){
    m_jours = 0;
    m_mois = 0;
    m_annees = 0;
}

Date::Date(unsigned int jours, unsigned int mois, unsigned int annees){
    Date();
    ajouteJours(jours);
    ajouteMois(mois);
    ajouteAnnees(annees);
    
    cout << jours << mois << annees << endl;
}

Date::Date(const Date &d){
    Date(d.getj(), d.getm(), d.geta());
}


Date Date::lendemain() const{
    return Date(m_jours+1, m_mois, m_annees);
}

Duree Date::dureeJusquA(Date d) const{
    return Duree(d.getj() - m_jours + (d.getm() - m_mois)*31 + (d.geta() - m_annees)*365);
}

void Date::ajouteJours(unsigned int nj){
    // ATTENTION QUE DES MOIS DE 31 jours...
    ajouteMois((m_jours + nj) / 31);
    m_jours = (m_jours + nj) % 31;
}

void Date::ajouteMois(unsigned int nm){
    ajouteAnnees((m_mois + nm) / 12);
    m_mois = (m_mois + nm) % 12;
}

void Date::ajouteAnnees(unsigned int na){
    m_annees += na;
}

void Date::enleveJours(unsigned int nj){
    // ATTENTION QUE DES MOIS DE 31 jours...
    enleveMois(abs((int)(m_jours - nj)) / 31);
    m_jours = abs((int)(m_jours - nj)) % 31;
}

void Date::enleveMois(unsigned int nm){
    enleveAnnees(abs((int)(m_mois - (int)nm)) / 12);
    m_mois = abs((int)(m_mois - nm)) % 12;
}

void Date::enleveAnnees(unsigned int na){
    m_annees -= na;
}

unsigned int Date::getj() const {
    return m_jours;
}

unsigned int Date::getm() const {
    return m_mois;
}

unsigned int Date::geta() const {
    return m_annees;
}

void Date::afficher() const {
    cout << "Date: " << m_jours << "/" << m_mois << "/" << m_annees << endl;
}