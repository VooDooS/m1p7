//
//  Duree.h
//  TP2-2013
//
//  Created by Ulysse Gérard on 28/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP2_2013__Duree__
#define __TP2_2013__Duree__

#include <iostream>

class Duree {
private:
    int m_njours;
    
public:
    Duree(int nj);
    Duree();
    
    void multiplierPar(int n);
    int getj() const;
    void afficher() const;
};

#endif /* defined(__TP2_2013__Duree__) */
