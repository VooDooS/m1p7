//
//  Date.h
//  TP2-2013
//
//  Created by Ulysse Gérard on 28/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP2_2013__Date__
#define __TP2_2013__Date__

#include <iostream>

#include "Duree.h"

class Date
{
private:
    unsigned int m_jours;
    unsigned int m_mois;
    unsigned int m_annees;
    
public:
    Date();
    Date(unsigned int jours, unsigned int mois, unsigned int annees);
    Date(const Date &d);
    
    Date lendemain() const;
    
    Duree dureeJusquA(Date d) const;
    
    void ajouteJours(unsigned int nj);
    void ajouteMois(unsigned int nm);
    void ajouteAnnees(unsigned int na);
    
    void enleveJours(unsigned int nj);
    void enleveMois(unsigned int nm);
    void enleveAnnees(unsigned int na);
    
    unsigned int getj() const;
    unsigned int getm() const;
    unsigned int geta() const;
    void afficher() const;
};

#endif /* defined(__TP2_2013__Date__) */
