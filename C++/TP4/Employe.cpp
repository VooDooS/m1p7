//
//  Employe.cpp
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Employe.h"

using namespace std;

Employe::Employe(string n, string p)
: m_nom(n), m_prenom(p) {
    m_dateEmbauche = "tototata";
    
    cout << "Création de l'employé " << n << endl;
}

bool Employe::operator== (const Employe &e) const{
    //On considère que deux Employes sont identiques s'ils ont même nom et même prénom....
    return (m_nom == e.getNom() && m_prenom == e.getPrenom());
}

vector<Client>* Employe::getClients() {
    return &m_clients;
}

bool Employe::estClient(Client &c) const {
    return (find(m_clients.begin(), m_clients.end(), c) != m_clients.end());
}

void Employe::ajoutClient(Client &c) {
    m_clients.push_back(c);
    
    cout << "Ajout du client " << c.getNom() << " à l'employé " << m_nom << endl;
}

void Employe::ajoutCompte(Client &c, int solde) const {
    if(estClient(c)){
        c.nouvCompte(solde);
    }
}

string Employe::getNom() const {
    return m_nom;
}

string Employe::getPrenom() const {
    return m_prenom;
}