//
//  main.cpp
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include <iostream>


#include "Banque.h"
#include "Agence.h"
#include "Employe.h"
#include "Client.h"
#include "Compte.h"

using namespace std;

int main(int argc, const char * argv[])
{
    
    Banque& cestlabanque = Banque::getInstance();
    Employe e1("Robert", "Demoyencourt");
    Employe e2("Michel", "Latourte");
    Client  c1("Toto", "Jean-louis");
    Client  c2("Tata", "Jean-michel");
    Client  c3("Titi", "Jean-marie");
    cout << endl;
    
    cestlabanque.ajoutAgence("21 rue des Tiercelins");
    cestlabanque.ajoutAgence("3 bd Saint Michel");
    cout << endl;
    
    cestlabanque.getAgences()[0].ajoutEmploye(e1);
    cestlabanque.getAgences()[1].ajoutEmploye(e2);
    cout << endl;
    
    e1.ajoutClient(c1);
    e1.ajoutCompte(c1, 345);
    cout << endl;
    
    e1.ajoutClient(c2);
    e1.ajoutCompte(c2, 199009.3);
    cout << endl;
    e2.ajoutClient(c3);
    e2.ajoutCompte(c3, 123);
    cout << endl;
    
    vector<Compte*> cpts = cestlabanque.getAgence(0).getComptes();
    
    cout << cpts.size() << " comptes trouvés:" << endl;
    
    for(int i=0; i < cpts.size(); i++){
        cout << "Client " << cpts[i]->getClient().getNom();
        cout << " possède compte de solde " << cpts[i]->getSolde() << endl;
    }
    
    cestlabanque.getAgences()[0].mutation(e1, cestlabanque.getAgences()[1]);
    return 0;
}

