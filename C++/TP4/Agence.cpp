//
//  Agence.cpp
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Agence.h"

using namespace std;

Agence::Agence(unsigned int n, string a) : m_numAgence(n), m_adresse(a) {
    cout << "Création de l'agence " << n << ": " << m_adresse << endl;
}

void Agence::mutation(Employe &e, Agence &a) {
    //Si c'est bien un employé de cette agence alors on le vire et on l'ajoute à l'autre agence:
    vireEmploye(e);
    a.ajoutEmploye(e);
}


vector<Employe>& Agence::getEmployes(){
    return m_employes;
}

vector<Compte*> Agence::getComptes(){
    vector<Compte*> cpts;
    for(int i=0; i < m_employes.size(); i++){
        cout << "employe " << m_employes[i].getNom() <<  " trouvé" << endl;
        vector<Client>* clts = m_employes[i].getClients();
        cout << ((*m_employes[i].getClients()))[0].getNom();
        
        for(int j=0; j < clts->size(); j++){
            cout << "client trouvé";
            vector<Compte>& cptc= (*clts)[j].getComptes();
            
            for(int k=0; k < cptc.size(); k++){
                cout << "COmpte trouvé";
                cpts.push_back(&cptc[k]);
            }
        }
    }
    return cpts;
}

bool Agence::estEmploye(Employe &e) const {
    return (find(m_employes.begin(), m_employes.end(), e) != m_employes.end());
}

void Agence::ajoutEmploye(Employe &e) {
    m_employes.push_back(e);
    
    cout << "Ajout de l'employé " << e.getNom() << " à l'agence " << m_adresse << endl;
}

void Agence::vireEmploye(Employe &e) {
    m_employes.erase(find(m_employes.begin(), m_employes.end(), e));
    
    cout << "Suppression de l'employé " << e.getNom() << " de l'agence " << m_adresse << endl;
}