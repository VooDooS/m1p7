//
//  Client.h
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP4__Client__
#define __TP4__Client__

#include <iostream>
#include <vector>

#include "Compte.h"
#include "Employe.h"

class Compte;

class Client {
private:
    std::string m_nom, m_prenom;
    std::vector<Compte> m_comptes;
    
    //Réservé aux employés !
    void nouvCompte(int solde);
    
public:
    Client(std::string nom, std::string prenom);
    bool operator== (const Client& d) const;
    const Compte& getCompteRef(int idCompte = 0) const;
    
    std::vector<Compte>& getComptes();
    
    std::string getNom() const;
    std::string getPrenom() const;
    
    //Employés amis !
    friend class Employe;
};

#endif /* defined(__TP4__Client__) */
