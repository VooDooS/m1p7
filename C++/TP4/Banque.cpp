//
//  Banque.cpp
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Banque.h"

using namespace std;

Banque::Banque() {
    m_nom = "Super banque";
    m_nomPdg = "Moi";
    m_adresse = "40 rue Curial";
    m_capital = 0;
    
    cout << "Création de la banque." << endl;
}

Banque& Banque::getInstance() {
    static Banque instance;
    
    return instance;
}

void Banque::ajoutAgence(string adresse){
    m_agences.push_back(Agence((unsigned int)m_agences.size(), adresse));
}

Agence& Banque::getAgence(unsigned int id) {
    if(m_agences.size() > id) {
        return m_agences[id];
    }
    exit(EXIT_FAILURE);
}

vector<Agence>& Banque::getAgences() {
    return m_agences;
}