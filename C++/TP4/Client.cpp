//
//  Client.cpp
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Client.h"

using namespace std;

Client::Client(string n, string p)
: m_nom(n), m_prenom(p){
    //Création d'un client
    cout << "Création du client " << n <<endl;
}

void Client::nouvCompte(int s) {
    m_comptes.push_back(Compte(*this, s));
    cout << "Ajout d'un compte à " << m_nom << endl;
}

bool Client::operator== (const Client& d) const{
    //On considère que deux clients sont identiques s'ils ont même nom et même prénom....
    return (m_nom == d.getNom() && m_prenom == d.getPrenom());
}

const Compte& Client::getCompteRef(int id) const{
    if(m_comptes.size() > id){
        return m_comptes[id];
    }
    else exit(EXIT_FAILURE);
}

vector<Compte>& Client::getComptes(){
    return m_comptes;
}

string Client::getNom() const {
    return m_nom;
}

string Client::getPrenom() const {
    return m_prenom;
}