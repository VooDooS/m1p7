//
//  Employe.h
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP4__Employe__
#define __TP4__Employe__

#include <iostream>
#include <vector>
#include <algorithm>

#include "Client.h"
class Client;
class Employe {
private:
    std::string m_nom, m_prenom, m_dateEmbauche;
    std::vector<Client> m_clients;
    
public:
    Employe(std::string nom, std::string prenom);
    
    bool operator==(const Employe &e) const;
    
    std::vector<Client>* getClients();
    
    bool estClient(Client &c) const;
    void ajoutClient(Client &c);
    void ajoutCompte(Client &c, int solde) const;
    std::string getNom() const;
    std::string getPrenom() const;
};

#endif /* defined(__TP4__Employe__) */
