//
//  Compte.h
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP4__Compte__
#define __TP4__Compte__

#include <iostream>

#include "Client.h"

class Client;

class Compte {
private:
    Client &m_client;
    double m_solde;
    
public:
    Compte(Client &c, int solde=0);
    bool operator==(const Compte&) const;
    bool operator!=(const Compte&) const;

    
    double getSolde() const;
    Client& getClient() const;
};

#endif /* defined(__TP4__Compte__) */
