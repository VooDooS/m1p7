//
//  Compte.cpp
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include "Compte.h"

Compte::Compte(Client &c, int solde) : m_client(c), m_solde(solde){
    // Création d'un compte
    std::cout << "Création d'un compte de solde " << solde << " pour le client " << c.getNom() << "." << std::endl;
}

bool Compte::operator==(const Compte &c) const {
    return !(m_client == c.m_client);
}

double Compte::getSolde() const{
    return m_solde;
}

Client& Compte::getClient() const {
    return m_client;
}
