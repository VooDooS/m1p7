//
//  Banque.h
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP4__Banque__
#define __TP4__Banque__

#include <iostream>
#include <vector>

#include "Agence.h"

//CLasse banque qui utilise le pattern singleton:
class Banque {
private:
    std::string m_nom;
    std::string m_nomPdg;
    std::string m_adresse;
    double m_capital;
    std::vector<Agence> m_agences;
    
    Banque();
    
    //On empèche les copies:
    Banque(Banque const &);
    void operator=(Banque const &);
    
public:
    static Banque& getInstance();
    void ajoutAgence(std::string adresse);
    
    Agence& getAgence(unsigned int id);
    std::vector<Agence>& getAgences();
};

#endif /* defined(__TP4__Banque__) */
