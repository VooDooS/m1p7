//
//  Agence.h
//  TP4
//
//  Created by Ulysse Gérard on 14/10/2013.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#ifndef __TP4__Agence__
#define __TP4__Agence__

#include <iostream>
#include <vector>

#include "Employe.h"
#include "Compte.h"

class Agence {
private:
    unsigned int m_numAgence;
    std::string m_adresse;
    std::vector<Employe> m_employes;
    
public:
    Agence(unsigned int num, std::string adresse);
    
    void mutation(Employe &e, Agence &a);
    
    std::vector<Employe>& getEmployes();
    std::vector<Compte*> getComptes();
    
    bool estEmploye(Employe &e) const;
    void ajoutEmploye(Employe &e);
    void vireEmploye(Employe &e);
};

#endif /* defined(__TP4__Agence__) */
