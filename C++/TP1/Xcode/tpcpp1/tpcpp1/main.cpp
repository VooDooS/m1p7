//
//  main.cpp
//  tpcpp1
//
//  Created by Ulysse Gérard on 23/09/13.
//  Copyright (c) 2013 Ulysse Gérard. All rights reserved.
//

#include <iostream>
#include <cmath>

using namespace std;

const int rad = 1;
const int deg = 2;
const int grd = 3;


/* EX 3 - 4 : Fonction qui calcule le sinus d'un angle donné en degrés, radians, ou grades.
Unité par défaut: degré. */
double sinus(double theta, int unit = deg){

    //Sinus veut l'angle en radians:
    if(unit == deg){
        //Conversion deg -> rad:
        theta = theta*(M_PI/180.);
    }
    else if (unit == grd){
        //Conversion grd -> rad:
        theta = theta*(M_PI/200.);
    }
    
    return sin(theta);
}


/* EX 5 : Nouvelles fonctions mon_plus */
// int , int -> int
int mon_plus(int n1, int n2){
    return n1 + n2;
}

// double , double -> double
double mon_plus(double n1, double n2){
    return n1 + n2;
}


/* EX 6 : copie */
int* copie(int t[], int n){
    int *t2 = new int[n];
    
    for(int i=0; i < n; i++){
        t2[i] = t[i];
    }
    
    return t2;
}


/* EX 7 : somme */
int somme(int t[], int n){
    int s = 0;
    
    for(int i=0; i < n; i++){
        s += t[i];
    }
    
    return s;
}
double somme(double t[], int n){
    double s = 0;
    
    for(int i=0; i < n; i++){
        s += t[i];
    }
    
    return s;
}


int main(int argc, const char * argv[])
{
    //EX 5
    short s1 = 1, s2 = 6;
    int n1 = 4, n2 = 76;
    float f1 = 7.8, f2 = 36.0;
    double x1 = 0.8, x2 = 36.78;
    
    mon_plus(n1, n2);
    mon_plus(n1, s2);
    mon_plus(f1, f2);
    mon_plus(x1, x2);
    //mon_plus(n1, x2);
    
    //EX 3 - 4
    cout << sinus(M_PI/2., rad) << endl;
    cout << sinus(30, deg) << endl;
    cout << sinus(180) << endl << endl;
    
    
    //EX 6
    int n = 10;
    int *t = new int[100];
    int *t2 = new int[n];
    
    for(int i=0; i < 100; i++){
        t[i] = i;
    }
    
    t2 = copie(t, n);
    
    for(int i=0; i < n; i++){
        cout << i << " ";
    }
    cout << endl;
    
    delete [] t;
    delete [] t2;
    
    
    //EX 7
    n = 99;
    int *t3 = new int[n];
    short *t4 = new short[n];
    double *t5 = new double[n];
    
    for(int i=0; i < n; i++) t3[i] = i;
    for(int i=0; i < n; i++) t4[i] = i;
    for(int i=0; i < n; i++) t5[i] = (double)i/2.;
    
    //cout << somme(t3, n) << " " << somme(t4, n) << " " << somme(t5, n) << " " << endl;
    cout << somme(t3, n) << " " << somme(t5, n) << " " << endl;
    return 0;
}

